package training.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;

public class CheckBoxesTest {

    private WebDriver driver;

    @BeforeMethod
    public void beforeTest () {
        System.setProperty("webdriver.chrome.chrome", "C:/drivers/chromedriver.exe");
        driver = new ChromeDriver();

    }

    @Test
    public void checkBoxesTest() {
        driver.navigate().to("http://przyklady.javastart.pl:8095/checkboxes");

        WebElement checkBox1 = driver.findElement(By.xpath("//*[@id='checkboxes']/input[1]"));
        WebElement checkBox2 = driver.findElement(By.xpath("//*[@id='checkboxes']/input[2]"));

        assertFalse(checkBox1.isSelected());
        assertTrue(checkBox2.isSelected());

        checkBox1.click();
        checkBox2.click();

        assertTrue(checkBox1.isSelected());
        assertFalse(checkBox2.isSelected());
    }

    @Test
    public void dropDownTest() {
        driver.navigate().to("http://przyklady.javastart.pl:8095/dropdown");

        WebElement dropdownWebElement = driver.findElement(By.id("dropdown"));
        Select select = new Select(dropdownWebElement);

        String selectedOption = select.getFirstSelectedOption().getText();
        assertEquals(selectedOption, "Please select an option");

        select.selectByValue("1");
        selectedOption = select.getFirstSelectedOption().getText();

        assertEquals(selectedOption, "Option 1");

        select.selectByValue("2");
        selectedOption = select.getFirstSelectedOption().getText();

        assertEquals(selectedOption, "Option 2");
    }

    @Test
    public void fileUploadTest() {
        driver.navigate().to("http://przyklady.javastart.pl:8095/upload");
        WebElement fileUpload = driver.findElement(By.id("file-upload"));
        WebElement upload = driver.findElement(By.id("file-submit"));
        fileUpload.sendKeys("C:/Users/sten/Desktop/testfile.txt");
        upload.click();
        WebElement uploaded = driver.findElement(By.id("uploaded-files"));
        String upl = uploaded.getText();
        assertEquals(upl, "testfile.txt");
    }

    @AfterMethod
    public void afterTest() {
        driver.close();
        driver.quit();
    }
}
