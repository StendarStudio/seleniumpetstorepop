package training.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.*;

public class SmileTest {

    private WebDriver driver;

    @BeforeMethod
    public void beforeTest() {
        System.setProperty("webdriver.chrome.chrome", "C:/drivers/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void smileTest() {
        driver.navigate().to("http://przyklady.javastart.pl/test/hover_mouse.html");
        WebElement smileyIcon1 = driver.findElement(By.id("smiley"));
        WebElement smileyIcon2 = driver.findElement(By.id("smiley2"));

        Actions action = new Actions(driver);

        action.moveToElement(smileyIcon1).moveToElement(smileyIcon2).build().perform();
        }

    @AfterMethod

    public void afterTest() {
        driver.close();
        driver.quit();
    }

}
