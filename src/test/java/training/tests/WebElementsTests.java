package training.tests;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;
import static java.lang.System.setOut;
import static java.lang.Thread.sleep;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;


public class WebElementsTests {

    private WebDriver driver;

    @BeforeMethod
    public void beforeTest() {
        System.setProperty("webdriver.chrome.chrome", "C:/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("http://przyklady.javastart.pl/test/full_form.html");
    }

    @Test
    public void typingIntoWebElementTest() throws InterruptedException {

        WebElement userNamieField = driver.findElement(By.id("username"));
        sleep(3000);
        userNamieField.sendKeys("Selenium Start");
        String typeUserNameValue = userNamieField.getAttribute("value"); /*getAttribute() - Metoda ta pobiera wartość atrybutu
                                                                            HTMLowego webelementu. Atrybutem będzie w tym wypadku value.
                                                                             Atrybut ten przechowuje wartości wpisane w polu User Name.

                                                                               */
        sleep(3000);
        assertEquals(typeUserNameValue, "Selenium Start");
    }

    @Test
    public void typingAndClearingValueInsideWebElementTest() throws InterruptedException {
        WebElement userNamieField = driver.findElement(By.id("username"));
        sleep(3000);
        userNamieField.sendKeys("Selenium Start");
        String typeUserNameValue = userNamieField.getAttribute("value");
        sleep(3000);
        assertEquals(typeUserNameValue, "Selenium Start");
        userNamieField.clear();
        sleep(3000);
        String emptyUserNameField = userNamieField.getAttribute("value");
        assertEquals(emptyUserNameField, "");
    }

    @Test
    public void checkRadioButtonTest() throws InterruptedException {


        WebElement maleRadioButton = driver.findElement(By.cssSelector("input[value='male']"));
        WebElement femaleRadioButton = driver.findElement(By.cssSelector("input[value='female']"));
        sleep(3000);
        maleRadioButton.click();
        sleep(3000);
        assertTrue(maleRadioButton.isSelected());

        femaleRadioButton.click();
        sleep(3000);
        assertTrue(femaleRadioButton.isSelected());
        sleep(3000);
        assertFalse(maleRadioButton.isSelected());
    }

    @Test
    public void checkBoxButtonTest() throws InterruptedException {
        driver.findElement(By.cssSelector("input[value='pizza']"));
        driver.findElement(By.cssSelector("input[value='spaghetti']"));
        driver.findElement(By.cssSelector("input[value='hamburger']"));

        WebElement pizzaCheckBox = driver.findElement(By.cssSelector("input[value='pizza']"));
        WebElement spaghettiCheckBox = driver.findElement(By.cssSelector("input[value='spaghetti']"));
        WebElement hamburgerCheckBox = driver.findElement(By.cssSelector("input[value='hamburger']"));

        pizzaCheckBox.click();
        sleep(3000);
        spaghettiCheckBox.click();
        sleep(3000);
        hamburgerCheckBox.click();
        sleep(3000);

        assertTrue(pizzaCheckBox.isSelected());
        assertTrue(spaghettiCheckBox.isSelected());
        assertTrue(hamburgerCheckBox.isSelected());

        pizzaCheckBox.click();
        sleep(3000);
        spaghettiCheckBox.click();
        sleep(3000);
        hamburgerCheckBox.click();
        sleep(3000);

        assertFalse(pizzaCheckBox.isSelected());
        assertFalse(spaghettiCheckBox.isSelected());
        assertFalse(hamburgerCheckBox.isSelected());
    }

    @Test
    public void dropDownListTest() throws InterruptedException {

        WebElement countryWebElement = driver.findElement(By.id("country"));
        Select countryDropDown = new Select(countryWebElement);

        List<WebElement> options = countryDropDown.getOptions();
        List<String> namesOfOption = new ArrayList<String>();

        for (WebElement option : options) {
            namesOfOption.add(option.getText());
            out.println(option.getText());
        }

        List<String> expectedNamesOfOptions = new ArrayList<String>();
        expectedNamesOfOptions.add("Germany");
        expectedNamesOfOptions.add("Poland");
        expectedNamesOfOptions.add("UK");

        assertEquals(namesOfOption, expectedNamesOfOptions);
    }

    @Test
    public void selectingOptionsFromDropDownTest() throws InterruptedException {

        WebElement countryWebElement = driver.findElement(By.id("country"));
        Select countryDropDown = new Select(countryWebElement);


         countryDropDown.selectByIndex(1);
         sleep(3000);
         assertEquals(countryDropDown.getFirstSelectedOption().getText(), "Poland");

         countryDropDown.selectByValue("de_DE");
         sleep(3000);
         assertEquals(countryDropDown.getFirstSelectedOption().getText(), "Germany");

         countryDropDown.selectByVisibleText("UK");
         sleep(3000);
         assertEquals(countryDropDown.getFirstSelectedOption().getText(), "UK");
    }

    @Test
    public void checkIfElementsOnPage() {

        WebElement userNameField = driver.findElement(By.id("username"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement emailLabel = driver.findElement(By.cssSelector("span[class='help-block']"));

        System.out.println("Is userNameField displayed " + userNameField.isDisplayed());
        System.out.println("Is userNameField enabled " + userNameField.isEnabled());
        System.out.println("Is passwordField displayed " + passwordField.isDisplayed());
        System.out.println("Is passwordField enabled " + passwordField.isEnabled());
        System.out.println("Is emailLabel displayed " + emailLabel.isDisplayed());
        System.out.println("Is emailLabel enabled " + emailLabel.isEnabled());

        assertTrue(userNameField.isDisplayed());
        assertTrue(passwordField.isDisplayed());
        assertTrue(emailLabel.isDisplayed());

        assertTrue(userNameField.isEnabled());
        assertFalse(passwordField.isEnabled());
    }


    @AfterMethod
    public void afterTest() {
        driver.close();
        driver.quit();
    }


}
